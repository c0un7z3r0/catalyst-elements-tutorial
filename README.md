# Tutorial To Demonstrate Use Cases For Catalyst-Elements Components

### Purpose

This is the reference repository for a tutorial that demonstrates common use cases for the [Catalyst-Elements](https://catalyst-elements.perpetualsummer.ltd) component library.

For a full walk-through of this tutorial please visit [The Catalyst-Elements Documentation Website](https://catalyst-elements.perpetualsummer.ltd/Tutorial)

### Quick Start

Clone this repo:

``` git clone https://c0un7z3r0@bitbucket.org/c0un7z3r0/catalyst-elements-tutorial.git ```

Install the dependencies (Requires [Node.js](https://nodejs.org/en/download/)):

``` npm i ```

Run The Application:

``` yarn start ```

### Contact

If you want to reach out with any requests or enquires please feel free to email me at: hello@anthonycregan.co.uk


----------------------

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).