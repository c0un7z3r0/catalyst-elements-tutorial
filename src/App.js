import { useState } from "react";
import "./App.css";
import GridContainer from "@perpetualsummer/catalyst-elements/GridContainer.js";
import GridCell from "@perpetualsummer/catalyst-elements/GridCell.js";
import FlexContainer from "@perpetualsummer/catalyst-elements/FlexContainer.js";

import UserNameInput from "./UserNameInput.js";
import UserAddressInput from "./UserAddressInput.js";
import UserCountryInput from "./UserCountryInput.js";
import countryOptions from "./countries.json";
import UserGenderInput from "./UserGenderInput.js";
import UserPasswordInput from "./UserPasswordInput.js";
import UserDateOfBirthInput from "./UserDobInput.js";
import UserHeightInput from "./UserHeightInput.js";
import UserQuestionaireInput from "./UserQuestionaireInput";
import UserEmailInput from "./UserEmailInput";
import UserSpamOptOutInput from "./UserSpamOptOutInput";
import UserSpamSwitchInput from "./UserSpamSwitchInput";
import UserDownloadButton from "./UserDownloadButton";
import UserSubmitButton from "./UserSubmitButton";
import ModalResultsDisplay from "./ModalResultsDisplay";
import UserFormProgress from "./UserFormProgress";
import ThemeContextProvider from "./ThemeContextProvider";

function App() {
  const [userName, setUserName] = useState("");
  const [userAddress, setUserAddress] = useState("");
  const [userCountry, setUserCountry] = useState("");
  const [userGender, setUserGender] = useState("");
  const [passwordField1, setPasswordField1] = useState("");
  const [passwordField2, setPasswordField2] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [userDob, setUserDob] = useState("");
  const [userHeight, setUserHeight] = useState(1.71);
  const [userAnswer, setUserAnswer] = useState(3);
  const [userEmailAddress, setUserEmailAddress] = useState("");
  const [userOptOut, setUserOptOut] = useState(false);
  const [userSpamSwitch, setUserSpamSwitch] = useState(false);
  const [userSubmittedData, setUserSubmittedData] = useState({});
  const [uploadingData, setUploadingData] = useState(false);

  const answerArray = [
    { value: 1, label: "Strongly Disagree" },
    { value: 2, label: "Disagree" },
    { value: 3, label: "Undecided" },
    { value: 4, label: "Agree" },
    { value: 5, label: "Strongly Agree" },
  ];

  const buttonOptions = [
    {
      label: "Download (Windows)",
      action: () => alert("Windows Download Selected"),
    },
    {
      label: "Download (MacOs)",
      action: () => alert("MacOS Download Selected"),
    },
    {
      label: "Download (Linux)",
      action: () => alert("Linux Download Selected"),
    },
  ];

  const submitFunction = () => {
    setUploadingData(true);
    setTimeout(() => {
      setUserSubmittedData({
        name: userName,
        address: userAddress,
        country: userCountry,
        gender: userGender,
        password: userPassword,
        dateOfBirth: userDob,
        height: userHeight,
        answer: answerArray[userAnswer - 1].label,
        emailAddress: userEmailAddress,
        optOut: userOptOut,
        partnerOptOut: userSpamSwitch,
      });
      setUploadingData(false);
    }, 1500);
  };

  const flushData = () => {
    setUserName("");
    setUserAddress("");
    setUserCountry("");
    setUserGender("");
    setUserPassword("");
    setPasswordField1("");
    setPasswordField2("");
    setUserDob("");
    setUserHeight(1.71);
    setUserAnswer(3);
    setUserEmailAddress("");
    setUserOptOut(false);
    setUserSpamSwitch(false);
    setUserSubmittedData({});
    setUploadingData(false);
  };

  const progress = () => {
    let progressValue = 0;
    if (userName !== "") {
      progressValue += 11;
    }
    if (userAddress !== "") {
      progressValue += 11;
    }
    if (userCountry !== "") {
      progressValue += 11;
    }
    if (userGender !== "") {
      progressValue += 11;
    }
    if (userPassword !== "") {
      progressValue += 11;
    }
    if (userDob !== "") {
      progressValue += 11;
    }
    if (userHeight !== 1.71) {
      progressValue += 11;
    }
    if (userAnswer !== 3) {
      progressValue += 11;
    }
    if (userEmailAddress !== "") {
      progressValue += 12;
    }
    return progressValue;
  };

  return (
    <ThemeContextProvider>
      <div className="App">
        <UserFormProgress progress={progress()} />
        <GridContainer
          grid="grid"
          columns="1fr 800px 1fr"
          rows="1fr"
          style={{ height: "100vh" }}
        >
          <GridCell
            columnStart="2"
            columnEnd="3"
            padding="20px"
            style={{ backgroundColor: "#eee" }}
          >
            <h1>Registration Form</h1>
            <FlexContainer
              justifyContent="flex-start"
              alignItems="flex-end"
              alignContent="center"
              wrap="wrap"
            >
              <UserNameInput userName={userName} setUserName={setUserName} />
              <UserAddressInput
                userAddress={userAddress}
                setUserAddress={setUserAddress}
              />
              <UserCountryInput
                userCountry={userCountry}
                setUserCountry={setUserCountry}
                countryOptions={countryOptions}
              />
              <UserGenderInput
                userGender={userGender}
                setUserGender={setUserGender}
              />
              <UserPasswordInput
                userPassword={userPassword}
                setUserPassword={setUserPassword}
                passwordField1={passwordField1}
                setPasswordField1={setPasswordField1}
                passwordField2={passwordField2}
                setPasswordField2={setPasswordField2}
              />
              <UserDateOfBirthInput userDob={userDob} setUserDob={setUserDob} />
              <UserHeightInput
                userHeight={userHeight}
                setUserHeight={setUserHeight}
              />
              <UserQuestionaireInput
                userAnswer={userAnswer}
                setUserAnswer={setUserAnswer}
                answerArray={answerArray}
              />
              <UserEmailInput
                userEmailAddress={userEmailAddress}
                setUserEmailAddress={setUserEmailAddress}
              />
              <UserSpamOptOutInput
                userOptOut={userOptOut}
                setUserOptOut={setUserOptOut}
              />
              <UserSpamSwitchInput
                userSpamSwitch={userSpamSwitch}
                setUserSpamSwitch={setUserSpamSwitch}
              />
              <UserDownloadButton buttonOptions={buttonOptions} />
              <UserSubmitButton
                submitFunction={submitFunction}
                uploadingData={uploadingData}
              />
            </FlexContainer>
          </GridCell>
        </GridContainer>
        <ModalResultsDisplay
          userSubmittedData={userSubmittedData}
          flushData={flushData}
        />
      </div>
    </ThemeContextProvider>
  );
}

export default App;
