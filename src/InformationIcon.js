import React from "react";

const InformationIcon = ({ width = "34px", height = "34px" }) => {
  return (
    <svg
      width={width}
      height={height}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 246 246"
    >
      <g data-name="Layer 2">
        <g data-name="layer4">
          <path
            fill="#fff"
            stroke="#3a73ef"
            strokeMiterlimit="10"
            strokeWidth="10"
            d="M241 123A118 118 0 11123 5a118 118 0 01118 118z"
            data-name="path2175"
          ></path>
        </g>
      </g>
      <g>
        <g>
          <path
            fill="#3a73ef"
            d="M225 123A102 102 0 11123 21a102 102 0 01102 102z"
          ></path>
        </g>
        <g>
          <g fill="#fff">
            <path d="M118.31 186.08c5.71-3.32 14.93-14.58 18.36-22.42l-3.53-1.5c-7.1 12.42-18.11 21.54-14.43 6.17l21.75-68.28c-5.68-.47-34.45 4.13-38.95 5l-.71 3.78c9.81.34 11.15 4.35 10.44 7.64l-16.31 52.11s-2.05 5.89-2 10.27c.18 10.01 13.18 13.86 25.38 7.23z"></path>
            <path d="M146.68 70.84a13.89 13.89 0 11-13.89-13.9 13.89 13.89 0 0113.89 13.9z"></path>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default InformationIcon;
