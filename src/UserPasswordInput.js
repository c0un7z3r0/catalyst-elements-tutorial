import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import InputPassword from "@perpetualsummer/catalyst-elements/InputPassword.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserPasswordInput = ({
  userPassword,
  setUserPassword,
  passwordField1,
  setPasswordField1,
  passwordField2,
  setPasswordField2,
}) => {
  const currentTheme = useContext(ThemeContext);

  const password1ChangeHandler = (password1) => {
    setPasswordField1(password1);
    if (passwordField1 !== "" && password1 === passwordField2) {
      setUserPassword(password1);
    } else {
      setUserPassword("");
    }
  };

  const password2ChangeHandler = (password2) => {
    setPasswordField2(password2);
    if (passwordField1 !== "" && passwordField1 === password2) {
      setUserPassword(passwordField1);
    } else {
      setUserPassword("");
    }
  };

  return (
    <>
      <FlexCell p={1}>
        <InputPassword
          label="Create Your Password"
          value={passwordField1}
          onChange={(e) => password1ChangeHandler(e.target.value)}
          enableReveal
          valid={passwordField1 !== "" && passwordField1 === passwordField2}
          icon={
            <Tooltip
              alignment="right"
              tooltipContent="Please Choose A Password."
            >
              <InformationIcon />
            </Tooltip>
          }
          theme={currentTheme.theme}
        />
      </FlexCell>
      <FlexCell p={1}>
        <InputPassword
          label="Re-type Your Password"
          value={passwordField2}
          onChange={(e) => password2ChangeHandler(e.target.value)}
          enableReveal
          warning={passwordField2 !== "" && passwordField1 !== passwordField2}
          alert={passwordField1 !== "" && passwordField2 === ""}
          valid={passwordField1 !== "" && passwordField1 === passwordField2}
          icon={
            <Tooltip
              alignment="right"
              tooltipContent="Please Re-type your password. This check is done to ensure there are no accidental typos"
            >
              <InformationIcon />
            </Tooltip>
          }
          theme={currentTheme.theme}
        />
      </FlexCell>
      {passwordField1 !== "" && passwordField1 === passwordField2 && (
        <span style={{ color: "green" }}>
          <sup>New Password Set</sup>
        </span>
      )}
      {passwordField1 !== "" && passwordField1 !== passwordField2 && (
        <span style={{ color: "red" }}>
          <sup>Passwords Do Not Match</sup>
        </span>
      )}
      {passwordField1 === "" && passwordField2 === "" && (
        <span style={{ color: "red" }}>
          <sup>Password Cannot Be Empty</sup>
        </span>
      )}
    </>
  );
};

export default UserPasswordInput;
