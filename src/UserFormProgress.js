import ProgressBar from "@perpetualsummer/catalyst-elements/ProgressBar";

const UserFormProgress = ({ progress }) => {
  return (
    <div className="progressBar">
      <ProgressBar
        max={100}
        value={progress}
        displayOutput={false}
        theme="minimal"
        valid
      />
    </div>
  );
};
export default UserFormProgress;
