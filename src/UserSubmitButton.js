import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import Button from "@perpetualsummer/catalyst-elements/Button.js";
import LoadingSpinner from "@perpetualsummer/catalyst-elements/LoadingSpinner.js";

const UserSubmitButton = ({ submitFunction, uploadingData }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <>
      <FlexCell p={1} xl={3} lg={2} md={3} sm={4} xs={4}>
        <Button
          onClick={() => submitFunction()}
          disabled={uploadingData}
          theme={currentTheme.theme}
        >
          REGISTER
        </Button>
      </FlexCell>
      {uploadingData && (
        <>
          <FlexCell p={1} xl={3} lg={1} md={2} sm={3} xs={3}>
            <LoadingSpinner size={50} />
          </FlexCell>
        </>
      )}
    </>
  );
};

export default UserSubmitButton;
