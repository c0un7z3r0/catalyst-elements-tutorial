import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import InputSelect from "@perpetualsummer/catalyst-elements/InputSelect.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserCountryInput = ({ userCountry, setUserCountry, countryOptions }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <FlexCell p={1}>
      <InputSelect
        value={userCountry}
        label="Select Country"
        id="InputSelectId"
        options={countryOptions}
        size={50}
        onChange={(e) => {
          setUserCountry(e.target.value);
        }}
        icon={
          <Tooltip
            alignment="right"
            tooltipContent="Please Select your Country of residence."
          >
            <InformationIcon />
          </Tooltip>
        }
        theme={currentTheme.theme}
      />
    </FlexCell>
  );
};

export default UserCountryInput;
