import Modal from "@perpetualsummer/catalyst-elements/Modal.js";
import Table from "@perpetualsummer/catalyst-elements/Table.js";

const ModalResultsDisplay = ({ userSubmittedData, flushData }) => {
  const isDataPopulated = () => {
    return Object.keys(userSubmittedData).length > 0;
  };

  const columnArray = [
    { key: "item", label: "Item" },
    { key: "value", label: "User Input" },
  ];

  let mappedData = Object.keys(userSubmittedData).map((item) => {
    return {
      item: item,
      value:
        typeof userSubmittedData[item] === "string"
          ? userSubmittedData[item]
          : JSON.stringify(userSubmittedData[item]),
    };
  });

  return (
    <Modal
      heading={"Thank You For Registering"}
      isOpen={isDataPopulated()}
      onClose={() => flushData()}
    >
      <Table colHeadings={columnArray} tableData={mappedData} width="600px" />
    </Modal>
  );
};

export default ModalResultsDisplay;
