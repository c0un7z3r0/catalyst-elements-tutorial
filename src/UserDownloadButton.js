import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import MultiButton from "@perpetualsummer/catalyst-elements/MultiButton.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserDownloadButton = ({ buttonOptions }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <FlexCell p={1} style={{ display: "flex", alignItems: "center" }}>
      <MultiButton
        options={buttonOptions}
        id="mock-download-app"
        customStyle={{ containerStyles: { marginRight: "10px" } }}
        theme={currentTheme.theme}
      />
      <Tooltip
        alignment="right"
        tooltipContent="Select your platform then click Download"
      >
        <InformationIcon />
      </Tooltip>
    </FlexCell>
  );
};

export default UserDownloadButton;
