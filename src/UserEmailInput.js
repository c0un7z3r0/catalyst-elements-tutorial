import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import InputEmail from "@perpetualsummer/catalyst-elements/InputEmail.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserEmailInput = ({ userEmailAddress, setUserEmailAddress }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <FlexCell p={1}>
      <InputEmail
        id="user-email-address"
        value={userEmailAddress}
        label="Email Address."
        onChange={(e) => setUserEmailAddress(e.target.value)}
        icon={
          <Tooltip
            alignment="right"
            tooltipContent="Please enter your email address. You can choose whether we spam you or not below."
          >
            <InformationIcon />
          </Tooltip>
        }
        theme={currentTheme.theme}
      />
    </FlexCell>
  );
};

export default UserEmailInput;
