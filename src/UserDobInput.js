import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import InputDate from "@perpetualsummer/catalyst-elements/InputDate.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserDateOfBirthInput = ({ userDob, setUserDob }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <FlexCell p={1}>
      <InputDate
        id="date-of-birth"
        label="Date of Birth"
        value={userDob}
        onChange={(e) => setUserDob(e.target.value)}
        icon={
          <Tooltip
            alignment="right"
            tooltipContent="Please provide your date of birth."
          >
            <InformationIcon />
          </Tooltip>
        }
        theme={currentTheme.theme}
      />
    </FlexCell>
  );
};

export default UserDateOfBirthInput;
