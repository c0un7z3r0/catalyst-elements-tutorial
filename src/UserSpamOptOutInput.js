import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import InputCheckbox from "@perpetualsummer/catalyst-elements/InputCheckbox.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserSpamOptOutInput = ({ userOptOut, setUserOptOut }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <FlexCell p={1}>
      <InputCheckbox
        id="spam-opt-out"
        label="Opt Out of Promotional Emails."
        value="opt-out"
        name="spam-opt-out"
        onChange={() => setUserOptOut(!userOptOut)}
        checked={userOptOut}
        theme={currentTheme.theme}
      />
      <Tooltip
        alignment="right"
        tooltipContent="Would you like to opt out of our Promotional Emails?"
      >
        <InformationIcon />
      </Tooltip>
    </FlexCell>
  );
};

export default UserSpamOptOutInput;
