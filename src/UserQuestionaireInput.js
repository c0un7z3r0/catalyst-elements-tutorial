import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import InputRange from "@perpetualsummer/catalyst-elements/InputRange.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserQuestionaireInput = ({ userAnswer, setUserAnswer, answerArray }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <FlexCell p={1}>
      <InputRange
        id="questionare"
        min={1}
        max={answerArray.length}
        step={1}
        value={userAnswer}
        label="Do you agree that Catalyst Elements is a good UI library?"
        onChange={(e) => setUserAnswer(e.target.value)}
        datalist={answerArray}
        displayOutput={false}
        icon={
          <Tooltip
            alignment="right"
            tooltipContent="Please share your opinion on the UI element library on this form."
          >
            <InformationIcon />
          </Tooltip>
        }
        theme={currentTheme.theme}
      />
    </FlexCell>
  );
};

export default UserQuestionaireInput;
