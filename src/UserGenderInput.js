import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import InputRadio from "@perpetualsummer/catalyst-elements/InputRadio.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserCountryInput = ({ userGender, setUserGender }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <FlexCell p={1}>
      <InputRadio
        label="Male"
        value="Male"
        name="Gender"
        onChange={(e) => setUserGender(e.target.value)}
        checked={userGender === "Male"}
        theme={currentTheme.theme}
      />
      <InputRadio
        label="Female"
        value="Female"
        name="Gender"
        onChange={(e) => setUserGender(e.target.value)}
        checked={userGender === "Female"}
        theme={currentTheme.theme}
      />
      <InputRadio
        label="Other"
        value="Other"
        name="Gender"
        onChange={(e) => setUserGender(e.target.value)}
        checked={userGender === "Other"}
        theme={currentTheme.theme}
      />
      <Tooltip alignment="right" tooltipContent="Please Choose your Gender.">
        <InformationIcon />
      </Tooltip>
    </FlexCell>
  );
};

export default UserCountryInput;
