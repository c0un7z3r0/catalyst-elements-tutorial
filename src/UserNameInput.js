import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import InputText from "@perpetualsummer/catalyst-elements/InputText.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserNameInput = ({ userName, setUserName }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <FlexCell p={1}>
      <InputText
        id="user-name"
        label="User Name"
        value={userName}
        onChange={(e) => setUserName(e.target.value)}
        icon={
          <Tooltip
            alignment="right"
            tooltipContent="Please choose a username. You will need this to login."
          >
            <InformationIcon />
          </Tooltip>
        }
        theme={currentTheme.theme}
      />
    </FlexCell>
  );
};

export default UserNameInput;
