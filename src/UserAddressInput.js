import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import InputTextarea from "@perpetualsummer/catalyst-elements/InputTextarea.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserAddressInput = ({ userAddress, setUserAddress }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <FlexCell p={1}>
      <InputTextarea
        id="user-address"
        label="Postal Address"
        value={userAddress}
        onChange={(e) => setUserAddress(e.target.value)}
        cols={21}
        rows={8}
        icon={
          <Tooltip
            alignment="right"
            tooltipContent="Please provide your postal address for delivery purposes."
          >
            <InformationIcon />
          </Tooltip>
        }
        theme={currentTheme.theme}
      />
    </FlexCell>
  );
};

export default UserAddressInput;
