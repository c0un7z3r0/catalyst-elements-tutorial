import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import InputSwitch from "@perpetualsummer/catalyst-elements/InputSwitch.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserSpamSwitchInput = ({ userSpamSwitch, setUserSpamSwitch }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <FlexCell p={1}>
      <InputSwitch
        id="spam-opt-out-switch"
        label="Opt Out of Promotional Emails From Our Partners."
        value="partner-opt-out"
        name="spam-partner-opt-out"
        onChange={() => setUserSpamSwitch(!userSpamSwitch)}
        checked={userSpamSwitch}
        warning={userSpamSwitch === false}
        valid={userSpamSwitch === true}
        theme={currentTheme.theme}
      />
      <Tooltip
        alignment="right"
        tooltipContent="Would you like to opt out of our partners Promotional Emails?"
      >
        <InformationIcon />
      </Tooltip>
    </FlexCell>
  );
};

export default UserSpamSwitchInput;
