import { useContext } from "react";
import { ThemeContext } from "./ThemeContextProvider";
import FlexCell from "@perpetualsummer/catalyst-elements/FlexCell.js";
import InputNumber from "@perpetualsummer/catalyst-elements/InputNumber.js";
import InformationIcon from "./InformationIcon.js";
import Tooltip from "@perpetualsummer/catalyst-elements/Tooltip.js";

const UserHeightInput = ({ userHeight, setUserHeight }) => {
  const currentTheme = useContext(ThemeContext);
  return (
    <FlexCell p={1}>
      <InputNumber
        id="height-in-meters"
        label="Your Height (in Meters)"
        size={30}
        min={0.56}
        max={2.72}
        step={0.01}
        value={userHeight}
        onChange={(e) => setUserHeight(e.target.value)}
        icon={
          <Tooltip
            alignment="right"
            tooltipContent="Please provide your Height in meters. Minimum = 0.56m. Maximum = 2.72m. If your height is smaller or larger than either of these values you should contact the Guiness Book of Records."
          >
            <InformationIcon />
          </Tooltip>
        }
        style={{ width: "190px" }}
        theme={currentTheme.theme}
      />
    </FlexCell>
  );
};

export default UserHeightInput;
